import styles from './Header.module.scss';
// import { NavLink } from 'react-router-dom';
import StarSvg from "../StarSvg"
import CartSvg from '../CartSvg';

const Header = () => {

  return (

    <header className={styles.header}>
      <span className={styles.logo}>Parfume shop</span>

      <nav>
        <ul>
          <li><a href='/home'>HOME</a></li>

          <li><div className={styles.svgWrapper}> <StarSvg /></div></li>

          <li><div className={styles.svgWrapper}><CartSvg /></div></li>


          {/* <a href='/card'>CARD</a> */}
          {/* <NavLink to="/" className={({ isActive }) => isActive && styles.active}>Home</NavLink>
            <NavLink to="/users" className={({ isActive }) => isActive && styles.active}>Users</NavLink> */}

        </ul>
      </nav>
    </header>
  )
}

export default Header;
