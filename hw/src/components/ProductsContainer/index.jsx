import PropTypes from 'prop-types';
import ProductCard from '../ProductCard';
import styles from './ProductsContainer.module.scss';


const ProductsContainer = ({ products = [], addToCart = () => { } }) => {
  return (
    
    <div className={styles.productsContainer}>
      {products && products.map(({ id, name, image, price }) => (

        <ProductCard
          key={id}
          name={name}
          image={image}
          price={price}
          addToCart={addToCart}
        />
      ))}
    </div>
  );
}

ProductsContainer.propTypes = {
  products: PropTypes.arrayOf(
    PropTypes.shape({
      price: PropTypes.number,
      id: PropTypes.number.isRequired,
      name: PropTypes.string,
      image: PropTypes.string,
    })
  ),
  addToCart: PropTypes.func,
};

export default ProductsContainer;




