import PropTypes from 'prop-types';
import styles from './ProductCard.module.scss';
import StarSvg from '../StarSvg';

function ProductCard({ name = "", image = "", price = 0, id = 0,  addToCart = () => { } }) {
  return (
    <div className={styles.productCard}>
      <div className={styles.svgWrapper}>
        <StarSvg />
      </div>

      <img src={image} alt={name} />
      <h2 className={styles.productName}>{name}</h2>
      <p className={styles.productPrice}>Price: {price}</p>

  


      {/* <button onClick={() => {addToCart(product)}} className={styles.addToCartBtn}>Add To Cart</button> */}
      <button onClick={() => { addToCart() }} className={styles.addToCartBtn}>Add To Cart</button>
    </div>
  );
}

// ProductCard.propTypes = {
//   product: PropTypes.shape({
//     id: PropTypes.number.isRequired,
//     name: PropTypes.string,
//     image: PropTypes.string,
//     price: PropTypes.number,
//     isFavourite: PropTypes.bool,
//   }),
//   addToCart: PropTypes.func,
//  

// };

export default ProductCard;