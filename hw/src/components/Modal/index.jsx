import PropTypes from "prop-types";

const Modal = ({ children, onClick = () => { } }) => {

    return (

        <div className="modal" onClick={onClick}>
            <div className="background" onClick={onClick}>{children}</div>
        </div>

    )
}


Modal.propTypes = {
    children: PropTypes.node,
    onClick: PropTypes.func,

}
export default Modal;
