import { useState, useEffect } from 'react';
import { useImmer } from "use-immer";
import axios from "axios";
import "./modal.scss";
import "./button.scss";
import Button from './components/Button'
import Modal from './components/Modal'
import ModalWrapper from './components/ModalWrapper';
import ModalHeader from './components/ModalHeader';
import ModalFooter from "./components/ModalFooter";
import ModalClose from "./components/ModalClose";
import ModalBody from "./components/ModalBody";
import Header from "./components/Header";
import ProductsContainer from './components/ProductsContainer';
// import ProductCard from './components/ProductCard';
// import { Routes, Route } from "react-router-dom";
// import CardContainer from './components/CardContainer';




function App() {

  const [products, setProducts] = useImmer([]);
  const [cart, setCart] = useImmer([])
  const [favourite, setFavourite] = useImmer([]);



  // const handleFavourite = (Id) => {

  //   // console.log(id);


  //   setProducts((draft) => {
  //     const product = draft.find(({ id }) => id === product.Id);
  //     console.log(product);
  //     product.isFavourite = !product.isFavourite;

  //   })
  // }





  useEffect(() => {
    const fetchProducts = async () => {
      try {
        const { data } = await axios.get('./products.json');
        setProducts(data);

      } catch (err) {
        console.log(err);
      }
    }

    fetchProducts();
  }, []);



  const imageUrl = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRC0czagZhuMjy2VUZnZQQopSvaiguo6CDE-FXi0AxybFhvTtu6Za4T1x4huw&s";

  const [isFirstModalOpen, setIsFirstModalOpen] = useState(false);
  const [isSecondModalOpen, setIsSecondModalOpen] = useState(false);


  const openFirstModal = () => {
    setIsFirstModalOpen(true);
  };

  const closeFirstModal = () => {
    setIsFirstModalOpen(false)
  }
  const openSecondModal = () => {
    setIsSecondModalOpen(true);
  };

  const closeSecondModal = () => {
    setIsSecondModalOpen(false)
  }



  return (
    <>

      {/* <div className='wrapper'>
        <Button onClick={() => { openFirstModal() }}>Open first modal</Button>
        <Button onClick={() => { openSecondModal() }}>Open second modal</Button>
      </div> */}
      <main>
        <Header />
        <ProductsContainer products={products} />

      </main>
      {isFirstModalOpen && (
        <Modal onClick={() => { closeFirstModal() }} children={<div className='background' onClick={() => { closeFirstModal() }}></div>}>
          <ModalWrapper onClick={(e) => { e.stopPropagation() }}>
            <ModalClose onClick={() => { closeFirstModal() }}></ModalClose>
            <ModalHeader children={<img className='modal__img' src={imageUrl} alt="image" />} />
            <ModalBody>
              <h2 className='header'>Product Delete!</h2>
              <p className='paragraph'>By clicking the “Yes, Delete” button, PRODUCT NAME will be deleted.</p>
            </ModalBody>
            <ModalFooter firstText="NO,CANCEL"
              secondaryText='YES,DELETE'
              firstClick={() => { }}
              secondaryClick={() => { }} />
          </ModalWrapper>

        </Modal>
      )}


      {isSecondModalOpen && (
        <Modal onClick={() => { closeSecondModal() }} children={<div className='background' onClick={() => { closeSecondModal() }}></div>}>
          <ModalWrapper onClick={(e) => { e.stopPropagation() }}>
            <ModalClose onClick={() => { closeSecondModal() }}></ModalClose>
            <ModalHeader children={<h3 className='header'>Add Product “NAME”</h3>} />
            <ModalBody>
              <p className='paragraph'>Description for you product</p>
            </ModalBody>
            <ModalFooter
              firstText={"ADD TO FAVORITE"}
              firstClick={() => { }} />
          </ModalWrapper>

        </Modal>


      )}



    </>
  )
}

export default App